<?php
namespace app\eventHandlers\beforeRequest;

class LocaleSetter
{
    /**
     * Priority:
     * 1)userSettings
     * 2)cookie
     * 3)browser
     */
    public static function setLocale($event)
    {
        \Yii::$app->language = self::userSettings()? : self::cookie()? : self::browser()? : \Yii::$app->language;
    }

    protected static function userSettings()
    {
        if(!\Yii::$app->user->isGuest){
            return Yii::$app->user->language;
        }
    }

    protected static function cookie()
    {
        return \Yii::$app->request->cookies->getValue('language', '');
    }

    protected static function browser()
    {
        $supportedLangs = scandir(\Yii::$app->basePath.'/messages');
        unset($supportedLangs[0], $supportedLangs[1]); // unset '.' and '..'
        $supportedLangs[] = 'en-US'; // default language
        $languages = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);
        foreach($languages as $lang){
            if(in_array($lang, $supportedLangs)) {
                // Set the page locale to the first supported language found
                return $lang;
            }
        }
    }
}
